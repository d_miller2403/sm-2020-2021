import React, { useState } from "react";
import Images from "./Images";
import "./styles.css";

export default function App() {
  const [selectedImg, setSelectedImg] = useState(Images[0]);

  return (
    <div className="App">
      <div className="container">
      <div class="text-container">
      <p>Dagmara Miller</p>
      <p>WITAM NA MOJEJ STRONIE PORTFOLIO</p>
      <p>Tutaj znajdują się moje zdjęcia.</p>
      <button class="hire-btn">Informacje</button>
      <button class="down-cv">Pobierz CV</button>
      </div>
        <img src={selectedImg} alt="Selected" className="selected" />
        <div className="imgContainer">
          {Images.map((img, index) => (
            <img
              style={{ border: selectedImg === img ? "4px solid white" : "" }}
              key={index}
              src={img}
              alt="zdjęcie, Dagmara Miller"
              onClick={() => setSelectedImg(img)}/>
          ))}
        </div>
      </div>
      </div>
  );
}
