import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import NavBar from "./NavBar";
import AboutBlurb from "./AboutBlurb";
import BottomBanner from "./BottomBanner";
import Footer from "./Footer";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <React.StrictMode>
    <NavBar />
    <App />
    <AboutBlurb />
    <BottomBanner />
    <Footer />
  </React.StrictMode>,
  rootElement
);
